import 'package:flutter/material.dart';
import 'package:virtual_platform/virtual_platform.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
  // mediaQueryDataNotifier = MediaQueryDataNotifier(data);
  virtualPlatformNotifier = VirtualPlatformNotifier(androidVirtualPlatform);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Virtual Platform Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            VirtualSubplatformDispatcher(
              other: () => TextButton(
                child: const Text('set to ios'),
                onPressed: () =>
                    virtualPlatformNotifier.chosenPlatform = iosVirtualPlatform,
              ),
              ios: () => TextButton(
                child: const Text('set to android'),
                onPressed: () => virtualPlatformNotifier.chosenPlatform =
                    androidVirtualPlatform,
              ),
            ),
            Builder(
              builder: (context) => Text(
                matchVirtualSubplatform(
                  context,
                  linux: () => "Linux",
                  androidSmartphone: () => "android s",
                  androidTabletLarge: () => "android l",
                  android: () => "android",
                  other: () => "other",
                ),
              ),
            ),
            VirtualSubplatformDispatcher(
              linux: () => const Text("Linux"),
              androidSmartphone: () => const Text("android s"),
              androidTabletLarge: () => const Text("android l"),
              android: () => const Text("android"),
              other: () => const Text("other"),
            ),
            VirtualPlatformDispatcher(
              linux: () => const Text("Linux"),
              android: () => const Text("android"),
              other: () => const Text("other"),
            ),
            Builder(
              builder: (_) => Text(
                matchVirtualPlatform(
                  linux: () => 'linux',
                  android: () => 'android',
                  other: () => 'other',
                ),
              ),
            ),
            ValueListenableBuilder<VirtualPlatform>(
              valueListenable: virtualPlatformNotifier,
              builder: (_, virtualPlatform, __) => Text(
                matchVirtualPlatform(
                  linux: () => 'linux',
                  android: () => 'android',
                  other: () => 'other',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
