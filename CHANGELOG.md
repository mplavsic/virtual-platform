## 0.2.0
Make it easier to distinguish platform groups only targeting platforms (by appending "Systems") and subplatform groups mostly taking into account the available screen size.
- Change platform groups to "appleSystems", "mobileSystems" and "desktopSystems".
- Change "apple" subplatform group to "appleSystems". Keep "mobile" and "desktop" subplatforms.

## 0.1.1
Pass static analysis and add docstring.

## 0.1.0
The package was split into 3 libraries. The documentation was rewritten.

## 0.0.1        
First release. It comes with:
* `matchPhysicalPlatform`
* `matchVirtualPlatform`
* `VirtualPlatformBuilder`
* `ResponsiveBuilder`
* `VirtualPlatformNotifier`
* `VirtualPlatform`