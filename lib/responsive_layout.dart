/// A library for using the responsive layout breakpoints and the responsive
/// dispatcher (for widgets).
library responsive_layout;

export 'src/responsive_layout/breakpoints.dart';
export 'src/responsive_layout/responsive_dispatcher.dart';
