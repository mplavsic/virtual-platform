import '../abstract/utils/typedefs.dart';
import 'virtual_platform.dart';
import 'match_virtual_platform_partially_applied.dart';

/// Immediately invokes a function matching the ***virtual*** platform.
///
/// You can use this in case you need to invoke some platform-specific
/// functions, e.g.:
///
/// * `showModalBottomSheet`
/// * `showCupertinoModalPopup`
///
/// **[other] chain**
///
/// [other] will be selected as default chain at runtime
/// in case the running platform (or a platform group containing it) is not
/// specified.
///
/// **Platform groups**
///
/// [appleSystems], [desktopSystems] and [mobileSystems] are platform groups.
///
/// - [mobileSystems] will be selected if, at runtime, the platform is one of:
///   - [android]
///   - [ios]
/// - [desktopSystems] will be selected if, at runtime, the platform is one of:
///   - [linux]
///   - [macos]
///   - [windows]
/// - [appleSystems] will be selected if, at runtime, the platform is one of:
///   - [ios]
///   - [macos]
///
/// Platforms have precedence over platform groups. [appleSystems] has precedence
/// over both [desktopSystems] and [mobileSystems].
T matchVirtualPlatform<T>({
  BuilderOf<T>? android,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? ios,
  BuilderOf<T>? linux,
  BuilderOf<T>? macos,
  BuilderOf<T>? windows,
  BuilderOf<T>? web,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? desktopSystems,
  BuilderOf<T>? mobileSystems,
  required BuilderOf<T> other,
}) =>
    matchVirtualPlatformPartiallyApplied(
      virtualPlatform: VirtualPlatform.current,
      android: android,
      appleSystems: appleSystems,
      desktopSystems: desktopSystems,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      mobileSystems: mobileSystems,
      other: other,
      web: web,
      windows: windows,
    );
