import 'package:flutter/material.dart' show Material;
import 'package:flutter/widgets.dart';

import '../abstract/utils/typedefs.dart';
import 'virtual_platform.dart';
import 'virtual_platform_notifier.dart';
import 'match_virtual_platform_partially_applied.dart';

/// Builds a [Widget] based on the virtual platform.
class VirtualPlatformDispatcher extends StatelessWidget {
  final BuilderOfWidget other;
  final NullableBuilderOfWidget android;
  final NullableBuilderOfWidget fuchsia;
  final NullableBuilderOfWidget ios;
  final NullableBuilderOfWidget linux;
  final NullableBuilderOfWidget macos;
  final NullableBuilderOfWidget windows;
  final NullableBuilderOfWidget web;
  final NullableBuilderOfWidget appleSystems;
  final NullableBuilderOfWidget desktopSystems;
  final NullableBuilderOfWidget mobileSystems;

  /// Selects the widget builder to invoke that matches the virtual platform.
  /// If the virtual platform changes, a rebuild will occur.
  /// Use this builder for ***platform-specific UI***.
  ///
  /// **[other] chain**
  ///
  /// [other] will be selected as default chain in case all
  /// other chains do not match.
  ///
  /// However, note that specifying [other] does not automatically bring safety,
  /// i.e.,
  /// a crash might occur even if the [other] chain is specified. For example,
  /// let us assume the virtual platform is [iosVirtualPlatform] and the widget
  /// tree consists of Cupertino widgets.
  /// If the matched chain is [other] and this chain builds a widget using the
  /// Material design, this widget should be wrapped in a [Material] before
  /// getting added to the "Cupertino" widget tree. If not, it might result
  /// in a crash.
  ///
  /// **Platform groups**
  ///
  /// [appleSystems], [desktopSystems] and [mobileSystems] are platform groups.
  ///
  /// - [mobileSystems] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktopSystems] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [appleSystems] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [appleSystems] has precedence
  /// over both [desktopSystems] and [mobileSystems].
  const VirtualPlatformDispatcher({
    super.key,
    required this.other,
    this.android,
    this.fuchsia,
    this.ios,
    this.linux,
    this.macos,
    this.windows,
    this.web,
    this.appleSystems,
    this.desktopSystems,
    this.mobileSystems,
  });

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<VirtualPlatform>(
      valueListenable: virtualPlatformNotifier,
      builder: (_, virtualPlatform, __) => matchVirtualPlatformPartiallyApplied(
        virtualPlatform: virtualPlatform,
        other: other,
        android: android,
        appleSystems: appleSystems,
        desktopSystems: desktopSystems,
        fuchsia: fuchsia,
        ios: ios,
        linux: linux,
        macos: macos,
        mobileSystems: mobileSystems,
        web: web,
        windows: windows,
      ),
    );
  }
}
