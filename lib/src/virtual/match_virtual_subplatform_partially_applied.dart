import 'package:flutter/cupertino.dart' hide Builder;
import 'package:virtual_platform/src/abstract/match_abstract_subplatform.dart';
import 'package:virtual_platform/virtual_platform.dart';

import '../abstract/utils/typedefs.dart';

/// Avoids having to repeat the following params:
///
/// ```dart
/// isAndroid: () => virtualPlatform == androidVirtualPlatform,
/// isIos: () => virtualPlatform == iosVirtualPlatform,
/// isFuchsia: () => virtualPlatform == fuchsiaVirtualPlatform,
/// isLinux: () => virtualPlatform == linuxVirtualPlatform,
/// isMacos: () => virtualPlatform == macosVirtualPlatform,
/// isWindows: () => virtualPlatform == windowsVirtualPlatform,
/// isWeb: () => virtualPlatform == webVirtualPlatform,
/// ```
T matchVirtualSubplatformPartiallyApplied<T>(
  BuildContext context, {
  required VirtualPlatform virtualPlatform,
  required bool treatAndroidDesktopAsTabletLarge,
  required bool treatIosDesktopAsTabletLarge,
  required bool treatSmallLinuxAsMobile,
  required bool treatSmallMacosAsMobile,
  required bool treatSmallWindowsAsMobile,
  BuilderOf<T>? mobile,
  BuilderOf<T>? tablet,
  BuilderOf<T>? tabletSmall,
  BuilderOf<T>? tabletLarge,
  BuilderOf<T>? android,
  BuilderOf<T>? androidSmartphone,
  BuilderOf<T>? androidTablet,
  BuilderOf<T>? androidTabletSmall,
  BuilderOf<T>? androidTabletLarge,
  BuilderOf<T>? androidDesktop,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? ios,
  BuilderOf<T>? iphone,
  BuilderOf<T>? ipad,
  BuilderOf<T>? ipadSmall,
  BuilderOf<T>? ipadLarge,
  BuilderOf<T>? iosDesktop,
  BuilderOf<T>? macos,
  BuilderOf<T>? macosSmartphone,
  BuilderOf<T>? macosTablet,
  BuilderOf<T>? macosTabletSmall,
  BuilderOf<T>? macosTabletLarge,
  BuilderOf<T>? macosDesktop,
  BuilderOf<T>? linux,
  BuilderOf<T>? linuxSmartphone,
  BuilderOf<T>? linuxTablet,
  BuilderOf<T>? linuxTabletSmall,
  BuilderOf<T>? linuxTabletLarge,
  BuilderOf<T>? linuxDesktop,
  BuilderOf<T>? windows,
  BuilderOf<T>? windowsSmartphone,
  BuilderOf<T>? windowsTablet,
  BuilderOf<T>? windowsTabletSmall,
  BuilderOf<T>? windowsTabletLarge,
  BuilderOf<T>? windowsDesktop,
  BuilderOf<T>? web,
  BuilderOf<T>? webSmartphone,
  BuilderOf<T>? webTablet,
  BuilderOf<T>? webTabletSmall,
  BuilderOf<T>? webTabletLarge,
  BuilderOf<T>? webDesktop,
  BuilderOf<T>? desktop,
  required BuilderOf<T> other,
  BuilderOf<T>? otherSmartphone,
  BuilderOf<T>? otherTablet,
  BuilderOf<T>? otherTabletSmall,
  BuilderOf<T>? otherTabletLarge,
  BuilderOf<T>? otherDesktop,
}) =>
    matchAbstractSubplatform(
      context,
      isAndroid: () => virtualPlatform == androidVirtualPlatform,
      isWeb: () => virtualPlatform == webVirtualPlatform,
      isIos: () => virtualPlatform == iosVirtualPlatform,
      isFuchsia: () => virtualPlatform == fuchsiaVirtualPlatform,
      isLinux: () => virtualPlatform == linuxVirtualPlatform,
      isMacos: () => virtualPlatform == macosVirtualPlatform,
      isWindows: () => virtualPlatform == windowsVirtualPlatform,
      treatAndroidDesktopAsTabletLarge: treatAndroidDesktopAsTabletLarge,
      treatIosDesktopAsTabletLarge: treatIosDesktopAsTabletLarge,
      treatSmallLinuxAsMobile: treatSmallLinuxAsMobile,
      treatSmallMacosAsMobile: treatSmallMacosAsMobile,
      treatSmallWindowsAsMobile: treatSmallWindowsAsMobile,
      other: other,
      android: android,
      androidSmartphone: androidSmartphone,
      androidTablet: androidTablet,
      androidTabletSmall: androidTabletSmall,
      androidTabletLarge: androidTabletLarge,
      androidDesktop: androidDesktop,
      appleSystems: appleSystems,
      desktop: desktop,
      fuchsia: fuchsia,
      ios: ios,
      ipad: ipad,
      ipadSmall: ipadSmall,
      ipadLarge: ipadLarge,
      iosDesktop: iosDesktop,
      iphone: iphone,
      linux: linux,
      linuxSmartphone: linuxSmartphone,
      linuxTablet: linuxTablet,
      linuxTabletSmall: linuxTabletSmall,
      linuxTabletLarge: linuxTabletLarge,
      linuxDesktop: linuxDesktop,
      macos: macos,
      macosSmartphone: macosSmartphone,
      macosTablet: macosTablet,
      macosTabletSmall: macosTabletSmall,
      macosTabletLarge: macosTabletLarge,
      macosDesktop: macosDesktop,
      mobile: mobile,
      tablet: tablet,
      tabletLarge: tabletLarge,
      tabletSmall: tabletSmall,
      web: web,
      webDesktop: webDesktop,
      webTablet: webTablet,
      webTabletSmall: webTabletSmall,
      webTabletLarge: webTabletLarge,
      webSmartphone: webSmartphone,
      windows: windows,
      windowsSmartphone: windowsSmartphone,
      windowsTablet: windowsTablet,
      windowsTabletSmall: windowsTabletSmall,
      windowsTabletLarge: windowsTabletLarge,
      windowsDesktop: windowsDesktop,
      otherSmartphone: otherSmartphone,
      otherTablet: otherTablet,
      otherTabletSmall: otherTabletSmall,
      otherTabletLarge: otherTabletLarge,
      otherDesktop: otherDesktop,
    );
