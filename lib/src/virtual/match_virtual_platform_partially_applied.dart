import '../abstract/match_abstract_platform.dart';
import '../abstract/utils/typedefs.dart';
import 'virtual_platform.dart';

/// Avoids having to repeat the following params:
///
/// ```dart
/// isAndroid: () => virtualPlatform == androidVirtualPlatform,
/// isIos: () => virtualPlatform == iosVirtualPlatform,
/// isFuchsia: () => virtualPlatform == fuchsiaVirtualPlatform,
/// isLinux: () => virtualPlatform == linuxVirtualPlatform,
/// isMacos: () => virtualPlatform == macosVirtualPlatform,
/// isWindows: () => virtualPlatform == windowsVirtualPlatform,
/// isWeb: () => virtualPlatform == webVirtualPlatform,
/// ```
T matchVirtualPlatformPartiallyApplied<T>({
  required VirtualPlatform virtualPlatform,
  BuilderOf<T>? android,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? ios,
  BuilderOf<T>? linux,
  BuilderOf<T>? macos,
  BuilderOf<T>? windows,
  BuilderOf<T>? web,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? desktopSystems,
  BuilderOf<T>? mobileSystems,
  required BuilderOf<T> other,
}) =>
    matchAbstractPlatform(
      isAndroid: () => virtualPlatform == androidVirtualPlatform,
      isWeb: () => virtualPlatform == webVirtualPlatform,
      isIos: () => virtualPlatform == iosVirtualPlatform,
      isFuchsia: () => virtualPlatform == fuchsiaVirtualPlatform,
      isLinux: () => virtualPlatform == linuxVirtualPlatform,
      isMacos: () => virtualPlatform == macosVirtualPlatform,
      isWindows: () => virtualPlatform == windowsVirtualPlatform,
      android: android,
      appleSystems: appleSystems,
      desktopSystems: desktopSystems,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      mobileSystems: mobileSystems,
      other: other,
      web: web,
      windows: windows,
    );
