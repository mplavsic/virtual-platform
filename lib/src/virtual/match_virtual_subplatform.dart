import 'package:flutter/cupertino.dart' hide Builder;

import '../abstract/utils/typedefs.dart';
import 'virtual_platform.dart';
import 'match_virtual_subplatform_partially_applied.dart';

/// Immediately invokes a function matching the ***virtual*** platform.
///
/// You can use this in case you need to invoke some subplatform-specific
/// functions, e.g.:
///
/// * `showModalBottomSheet` on smartphones
/// * `showDialog` on tablets and screens with bigger dimensions
///
/// **[other] chain**
///
/// If provided, [other] will also be selected as default chain at runtime
/// in case the running platform (or a platform group containing it) is not
/// specified. [other] will also be used if the platform is not yet supported by
/// Flutter or this plugin. If [other] is not assigned, a crash might occur.
///
/// **Platform groups**
///
/// [appleSystems], [desktop] and [mobile] are platform groups.
///
/// - [mobile] will be selected if, at runtime, the platform is one of:
///   - [android]
///   - [ios]
/// - [desktop] will be selected if, at runtime, the platform is one of:
///   - [linux]
///   - [macos]
///   - [windows]
/// - [appleSystems] will be selected if, at runtime, the platform is one of:
///   - [ios]
///   - [macos]
///
/// Platforms have precedence over platform groups. [appleSystems] has precedence
/// over both [desktop] and [mobile].
T matchVirtualSubplatform<T>(
  BuildContext context, {
  bool treatAndroidDesktopAsTabletLarge = true,
  bool treatIosDesktopAsTabletLarge = true,
  bool treatSmallLinuxAsMobile = true,
  bool treatSmallMacosAsMobile = true,
  bool treatSmallWindowsAsMobile = true,
  BuilderOf<T>? mobile,
  BuilderOf<T>? tablet,
  BuilderOf<T>? tabletSmall,
  BuilderOf<T>? tabletLarge,
  BuilderOf<T>? android,
  BuilderOf<T>? androidSmartphone,
  BuilderOf<T>? androidTablet,
  BuilderOf<T>? androidTabletSmall,
  BuilderOf<T>? androidTabletLarge,
  BuilderOf<T>? androidDesktop,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? ios,
  BuilderOf<T>? iphone,
  BuilderOf<T>? ipad,
  BuilderOf<T>? ipadSmall,
  BuilderOf<T>? ipadLarge,
  BuilderOf<T>? iosDesktop,
  BuilderOf<T>? macos,
  BuilderOf<T>? macosSmartphone,
  BuilderOf<T>? macosTablet,
  BuilderOf<T>? macosTabletSmall,
  BuilderOf<T>? macosTabletLarge,
  BuilderOf<T>? macosDesktop,
  BuilderOf<T>? linux,
  BuilderOf<T>? linuxSmartphone,
  BuilderOf<T>? linuxTablet,
  BuilderOf<T>? linuxTabletSmall,
  BuilderOf<T>? linuxTabletLarge,
  BuilderOf<T>? linuxDesktop,
  BuilderOf<T>? windows,
  BuilderOf<T>? windowsSmartphone,
  BuilderOf<T>? windowsTablet,
  BuilderOf<T>? windowsTabletSmall,
  BuilderOf<T>? windowsTabletLarge,
  BuilderOf<T>? windowsDesktop,
  BuilderOf<T>? web,
  BuilderOf<T>? webSmartphone,
  BuilderOf<T>? webTablet,
  BuilderOf<T>? webTabletSmall,
  BuilderOf<T>? webTabletLarge,
  BuilderOf<T>? webDesktop,
  BuilderOf<T>? desktop,
  required BuilderOf<T> other,
  BuilderOf<T>? otherSmartphone,
  BuilderOf<T>? otherTablet,
  BuilderOf<T>? otherTabletSmall,
  BuilderOf<T>? otherTabletLarge,
  BuilderOf<T>? otherDesktop,
}) =>
    matchVirtualSubplatformPartiallyApplied(
      context,
      virtualPlatform: VirtualPlatform.current,
      treatAndroidDesktopAsTabletLarge: treatAndroidDesktopAsTabletLarge,
      treatIosDesktopAsTabletLarge: treatIosDesktopAsTabletLarge,
      treatSmallLinuxAsMobile: treatSmallLinuxAsMobile,
      treatSmallMacosAsMobile: treatSmallMacosAsMobile,
      treatSmallWindowsAsMobile: treatSmallWindowsAsMobile,
      other: other,
      android: android,
      androidSmartphone: androidSmartphone,
      androidTablet: androidTablet,
      androidTabletSmall: androidTabletSmall,
      androidTabletLarge: androidTabletLarge,
      androidDesktop: androidDesktop,
      appleSystems: appleSystems,
      desktop: desktop,
      fuchsia: fuchsia,
      ios: ios,
      ipad: ipad,
      ipadSmall: ipadSmall,
      ipadLarge: ipadLarge,
      iosDesktop: iosDesktop,
      iphone: iphone,
      linux: linux,
      linuxSmartphone: linuxSmartphone,
      linuxTablet: linuxTablet,
      linuxTabletSmall: linuxTabletSmall,
      linuxTabletLarge: linuxTabletLarge,
      linuxDesktop: linuxDesktop,
      macos: macos,
      macosSmartphone: macosSmartphone,
      macosTablet: macosTablet,
      macosTabletSmall: macosTabletSmall,
      macosTabletLarge: macosTabletLarge,
      macosDesktop: macosDesktop,
      mobile: mobile,
      tablet: tablet,
      tabletLarge: tabletLarge,
      tabletSmall: tabletSmall,
      web: web,
      webDesktop: webDesktop,
      webTablet: webTablet,
      webTabletSmall: webTabletSmall,
      webTabletLarge: webTabletLarge,
      webSmartphone: webSmartphone,
      windows: windows,
      windowsSmartphone: windowsSmartphone,
      windowsTablet: windowsTablet,
      windowsTabletSmall: windowsTabletSmall,
      windowsTabletLarge: windowsTabletLarge,
      windowsDesktop: windowsDesktop,
      otherSmartphone: otherSmartphone,
      otherTablet: otherTablet,
      otherTabletSmall: otherTabletSmall,
      otherTabletLarge: otherTabletLarge,
      otherDesktop: otherDesktop,
    );
