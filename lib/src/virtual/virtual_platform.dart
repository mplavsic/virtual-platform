import 'dart:io';

import 'package:flutter/foundation.dart';

import '../physical/match_physical_platform.dart';
import 'virtual_platform_notifier.dart';

/// Needed for [androidVirtualPlatform], [fuchsiaVirtualPlatform],
/// [iosVirtualPlatform], [linuxVirtualPlatform], [macosVirtualPlatform],
/// [windowsVirtualPlatform], [webVirtualPlatform].
class VirtualPlatform {
  final String _name;

  const VirtualPlatform._(this._name);

  /// Useful when saving the used [VirtualPlatform] to persistent storage.
  @override
  String toString() => _name;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VirtualPlatform &&
          runtimeType == other.runtimeType &&
          _name == other._name;

  @override
  int get hashCode => _name.hashCode;

  /// Useful when saving the newly selected [VirtualPlatform] to persistent
  /// storage.
  ///
  /// NB: [virtualPlatformNotifier] needs to be initialized first.
  static VirtualPlatform get current => virtualPlatformNotifier.value;

  /// The virtual platform representation of the actual platform.
  static VirtualPlatform get physical =>
      fromString(kIsWeb ? 'web' : Platform.operatingSystem);

  /// Useful when loading the last used [VirtualPlatform] from persistent
  /// storage.
  ///
  /// It will default to the corresponding physical platform if the [platformName]
  /// is invalid.
  static VirtualPlatform fromString(String platformName) {
    switch (platformName) {
      case "android":
        return androidVirtualPlatform;
      case "fuchsia":
        return fuchsiaVirtualPlatform;
      case "ios":
        return iosVirtualPlatform;
      case "linux":
        return linuxVirtualPlatform;
      case "macos":
        return macosVirtualPlatform;
      case "windows":
        return windowsVirtualPlatform;
      case "web":
        return webVirtualPlatform;
      default:
        return matchPhysicalPlatform(
          android: () => androidVirtualPlatform,
          fuchsia: () => fuchsiaVirtualPlatform,
          ios: () => iosVirtualPlatform,
          linux: () => linuxVirtualPlatform,
          macos: () => macosVirtualPlatform,
          web: () => webVirtualPlatform,
          windows: () => windowsVirtualPlatform,
          // ignore: null_check_always_fails
          other: () => null!,
        );
    }
  }
}

/// One of the supported virtual platforms.
const androidVirtualPlatform = VirtualPlatform._('android');

/// One of the supported virtual platforms.
const fuchsiaVirtualPlatform = VirtualPlatform._('fuchsia');

/// One of the supported virtual platforms.
const iosVirtualPlatform = VirtualPlatform._('ios');

/// One of the supported virtual platforms.
const linuxVirtualPlatform = VirtualPlatform._('linux');

/// One of the supported virtual platforms.
const macosVirtualPlatform = VirtualPlatform._('macos');

/// One of the supported virtual platforms.
const windowsVirtualPlatform = VirtualPlatform._('windows');

/// One of the supported virtual platforms.
const webVirtualPlatform = VirtualPlatform._('web');
