import 'package:flutter/widgets.dart';

import 'virtual_platform.dart';

/// This notifier must be instantiated before it is listened to.
late final VirtualPlatformNotifier virtualPlatformNotifier;

class VirtualPlatformNotifier extends ValueNotifier<VirtualPlatform> {
  VirtualPlatformNotifier(VirtualPlatform init) : super(init);
  VirtualPlatformNotifier.physicalPlatform() : super(VirtualPlatform.physical);

  set chosenPlatform(VirtualPlatform platform) {
    value = platform;
    notifyListeners();
  }

  void setToPhysicalPlatform() {
    value = VirtualPlatform.physical;
    notifyListeners();
  }
}
