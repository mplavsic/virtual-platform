import 'package:flutter/cupertino.dart' hide Builder;

import '../responsive_layout/breakpoints.dart';
import 'utils/chain.dart';
import 'utils/typedefs.dart';

/// Immediately invokes a function matching the ***abstract*** subplatform.
///
/// **[other] chain**
///
/// [other] will be selected as default chain at runtime
/// in case the platform (or a platform group containing it) is not
/// specified.
///
/// **Subplatform groups**
///
/// To find out all possible combinations, the
/// [documentation](https://gitlab.com/mplavsic/virtual-platform)
/// should be consulted.
T matchAbstractSubplatform<T>(
  BuildContext context, {
  required bool Function() isAndroid,
  required bool Function() isIos,
  required bool Function() isFuchsia,
  required bool Function() isLinux,
  required bool Function() isMacos,
  required bool Function() isWindows,
  required bool Function() isWeb,
  bool treatAndroidDesktopAsTabletLarge = true,
  bool treatIosDesktopAsTabletLarge = true,
  bool treatSmallLinuxAsMobile = true,
  bool treatSmallMacosAsMobile = true,
  bool treatSmallWindowsAsMobile = true,
  BuilderOf<T>? mobile,
  BuilderOf<T>? tablet,
  BuilderOf<T>? tabletSmall,
  BuilderOf<T>? tabletLarge,
  BuilderOf<T>? android,
  BuilderOf<T>? androidSmartphone,
  BuilderOf<T>? androidTablet,
  BuilderOf<T>? androidTabletSmall,
  BuilderOf<T>? androidTabletLarge,
  BuilderOf<T>? androidDesktop,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? ios,
  BuilderOf<T>? iphone,
  BuilderOf<T>? ipad,
  BuilderOf<T>? ipadSmall,
  BuilderOf<T>? ipadLarge,
  BuilderOf<T>? iosDesktop,
  BuilderOf<T>? macos,
  BuilderOf<T>? macosSmartphone,
  BuilderOf<T>? macosTablet,
  BuilderOf<T>? macosTabletSmall,
  BuilderOf<T>? macosTabletLarge,
  BuilderOf<T>? macosDesktop,
  BuilderOf<T>? linux,
  BuilderOf<T>? linuxSmartphone,
  BuilderOf<T>? linuxTablet,
  BuilderOf<T>? linuxTabletSmall,
  BuilderOf<T>? linuxTabletLarge,
  BuilderOf<T>? linuxDesktop,
  BuilderOf<T>? windows,
  BuilderOf<T>? windowsSmartphone,
  BuilderOf<T>? windowsTablet,
  BuilderOf<T>? windowsTabletSmall,
  BuilderOf<T>? windowsTabletLarge,
  BuilderOf<T>? windowsDesktop,
  BuilderOf<T>? web,
  BuilderOf<T>? webSmartphone,
  BuilderOf<T>? webTablet,
  BuilderOf<T>? webTabletSmall,
  BuilderOf<T>? webTabletLarge,
  BuilderOf<T>? webDesktop,
  BuilderOf<T>? desktop,
  required BuilderOf<T> other,
  BuilderOf<T>? otherSmartphone,
  BuilderOf<T>? otherTablet,
  BuilderOf<T>? otherTabletSmall,
  BuilderOf<T>? otherTabletLarge,
  BuilderOf<T>? otherDesktop,
}) {
  final BuilderOf<T>? platformChain;

  final width = MediaQuery.of(context).size.width;

  BuilderOf<T>? partiallyAppliedSelectChain({
    required BuilderOf<T>? Function() smartphoneWidth,
    required BuilderOf<T>? Function() smallTabletWidth,
    required BuilderOf<T>? Function() tabletLargeWidth,
    required BuilderOf<T>? Function() desktopWidth,
  }) =>
      _selectChain(
        width: width,
        smartphoneWidth: smartphoneWidth,
        smallTabletWidth: smallTabletWidth,
        tabletLargeWidth: tabletLargeWidth,
        desktopWidth: desktopWidth,
        otherSmartphone: otherSmartphone,
        otherTablet: otherTablet,
        otherTabletSmall: otherTabletSmall,
        otherTabletLarge: otherTabletLarge,
        otherDesktop: otherDesktop,
      );

  if (isWeb()) {
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [webSmartphone, web, mobile],
      ),
      smallTabletWidth: () => chain(
        [webTabletSmall, webTablet, web, tabletSmall, tablet, mobile],
      ),
      tabletLargeWidth: () => chain(
        [webTabletLarge, webTablet, web, tabletLarge, tablet, mobile],
      ),
      desktopWidth: () => chain(
        [webDesktop, web, desktop],
      ),
    );
  } else if (isAndroid()) {
    BuilderOf<T>? maybe(BuilderOf<T>? builder) =>
        treatAndroidDesktopAsTabletLarge ? builder : null;
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [androidSmartphone, android, mobile],
      ),
      smallTabletWidth: () => chain(
        [
          androidTabletSmall,
          androidTablet,
          android,
          tabletSmall,
          tablet,
          mobile
        ],
      ),
      tabletLargeWidth: () => chain(
        [
          androidTabletLarge,
          androidTablet,
          android,
          tabletLarge,
          tablet,
          mobile
        ],
      ),
      desktopWidth: () => chain(
        [
          androidDesktop,
          maybe(androidTablet),
          android,
          maybe(tabletLarge),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
    );
  } else if (isIos()) {
    BuilderOf<T>? maybe(BuilderOf<T>? builder) =>
        treatIosDesktopAsTabletLarge ? builder : null;
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [iphone, ios, appleSystems, mobile],
      ),
      smallTabletWidth: () => chain(
        [ipadSmall, ipad, ios, appleSystems, tabletSmall, tablet, mobile],
      ),
      tabletLargeWidth: () => chain(
        [ipadLarge, ipad, ios, appleSystems, tabletLarge, tablet, mobile],
      ),
      desktopWidth: () => chain(
        [
          iosDesktop,
          maybe(ipad),
          ios,
          appleSystems,
          maybe(tabletLarge),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
    );
  } else if (isLinux()) {
    BuilderOf<T>? maybe(BuilderOf<T>? builder) =>
        treatSmallLinuxAsMobile ? builder : null;
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [linuxSmartphone, linux, maybe(mobile), desktop],
      ),
      smallTabletWidth: () => chain(
        [
          linuxTabletSmall,
          linuxTablet,
          linux,
          maybe(tabletSmall),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      tabletLargeWidth: () => chain(
        [
          linuxTabletLarge,
          linuxTablet,
          linux,
          maybe(tabletLarge),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      desktopWidth: () => chain(
        [linuxDesktop, linux, desktop],
      ),
    );
  } else if (isMacos()) {
    BuilderOf<T>? maybe(BuilderOf<T>? builder) =>
        treatSmallMacosAsMobile ? builder : null;
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [macosSmartphone, macos, appleSystems, maybe(mobile), desktop],
      ),
      smallTabletWidth: () => chain(
        [
          macosTabletSmall,
          macosTablet,
          macos,
          appleSystems,
          maybe(tabletSmall),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      tabletLargeWidth: () => chain(
        [
          macosTabletLarge,
          macosTablet,
          macos,
          appleSystems,
          maybe(tabletLarge),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      desktopWidth: () => chain(
        [macosDesktop, macos, appleSystems, desktop],
      ),
    );
  } else if (isWindows()) {
    BuilderOf<T>? maybe(BuilderOf<T>? builder) =>
        treatSmallWindowsAsMobile ? builder : null;
    platformChain = partiallyAppliedSelectChain(
      smartphoneWidth: () => chain(
        [windowsSmartphone, windows, maybe(mobile), desktop],
      ),
      smallTabletWidth: () => chain(
        [
          windowsTabletSmall,
          windowsTablet,
          windows,
          maybe(tabletSmall),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      tabletLargeWidth: () => chain(
        [
          windowsTabletLarge,
          windowsTablet,
          windows,
          maybe(tabletLarge),
          maybe(tablet),
          maybe(mobile),
          desktop
        ],
      ),
      desktopWidth: () => chain(
        [windowsDesktop, windows, desktop],
      ),
    );
  } else if (isFuchsia()) {
    platformChain = chain([fuchsia]);
  } else {
    platformChain = null;
  }
  // [platformChain] can be null, however, [other] can't.
  return chain([platformChain, other])!.call();
}

/// This function is the core of this package responsive layout dispatcher. It selects
/// the chain based on the width.
BuilderOf<T>? _selectChain<T>({
  required double width,
  required BuilderOf<T>? Function() smartphoneWidth,
  required BuilderOf<T>? Function() smallTabletWidth,
  required BuilderOf<T>? Function() tabletLargeWidth,
  required BuilderOf<T>? Function() desktopWidth,
  required BuilderOf<T>? otherSmartphone,
  required BuilderOf<T>? otherTablet,
  required BuilderOf<T>? otherTabletSmall,
  required BuilderOf<T>? otherTabletLarge,
  required BuilderOf<T>? otherDesktop,
}) {
  if (breakPoints.isSmartphone(width)) {
    return chain([smartphoneWidth(), otherSmartphone]);
  } else if (breakPoints.isTabletSmall(width)) {
    return chain([smallTabletWidth(), otherTabletSmall, otherTablet]);
  } else if (breakPoints.isTabletLarge(width)) {
    return chain([tabletLargeWidth(), otherTabletLarge, otherTablet]);
  } else {
    return chain([desktopWidth(), otherDesktop]);
  }
}
