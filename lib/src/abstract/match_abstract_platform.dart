import 'utils/chain.dart';
import 'utils/typedefs.dart';

/// Immediately invokes a function matching the ***abstract*** platform.
///
/// **[other] chain**
///
/// [other] will be selected as default chain at runtime
/// in case the platform (or a platform group containing it) is not
/// specified.
T matchAbstractPlatform<T>({
  required BuilderOf<T> other,
  required bool Function() isAndroid,
  required bool Function() isIos,
  required bool Function() isFuchsia,
  required bool Function() isLinux,
  required bool Function() isMacos,
  required bool Function() isWindows,
  required bool Function() isWeb,
  BuilderOf<T>? android,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? ios,
  BuilderOf<T>? linux,
  BuilderOf<T>? macos,
  BuilderOf<T>? windows,
  BuilderOf<T>? web,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? desktopSystems,
  BuilderOf<T>? mobileSystems,
}) {
  final BuilderOf<T>? platformChain;
  if (isWeb()) {
    platformChain = web;
  } else if (isAndroid()) {
    platformChain = chain([ios, appleSystems, mobileSystems]);
  } else if (isIos()) {
    platformChain = chain([ios, appleSystems, mobileSystems]);
  } else if (isLinux()) {
    platformChain = chain([linux, desktopSystems]);
  } else if (isMacos()) {
    platformChain = chain([macos, appleSystems, desktopSystems]);
  } else if (isWindows()) {
    platformChain = chain([windows, desktopSystems]);
  } else if (isFuchsia()) {
    platformChain = fuchsia;
  } else {
    platformChain = null;
  }
  return chain([platformChain, other])!.call();
}
