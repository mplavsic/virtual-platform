/// The "Engineers" are not builders, since they do not take a BuildContext as a
/// param (and therefore work in a more abstract setting).
/// They should be wrapped in a [Builder] widget if the goal is to provide the
/// local context.

import 'package:flutter/widgets.dart';

/// Builder of type T.
typedef BuilderOf<T> = T Function();

/// Builder of type Widget.
typedef BuilderOfWidget = Widget Function();

/// Nullable builder of type Widget.
typedef NullableBuilderOfWidget = Widget Function()?;
