import 'typedefs.dart';

BuilderOf<T>? chain<T>(List<BuilderOf<T>?> chain) {
  for (final next in chain) {
    if (next != null) return next;
  }
  return null;
}
