/// Default that can be replaced.
AbstractBreakpoints breakPoints = Breakpoints(600, 900, 1200);

abstract class AbstractBreakpoints {
  bool lessThan(double width, double logicalPixels) => width <= logicalPixels;

  //! In case a platform has way more logical pixels than the other ones,
  //! add the chain of that platform to the [matchPhysicalPlatform] below.

  bool isSmartphone(double width);

  bool isTabletSmall(double width);

  bool isTabletLarge(double width);
}

/// A simple implementation of [AbstractBreakpoints].
class Breakpoints extends AbstractBreakpoints {
  final double smartphoneMaxWidth;
  final double tabletSmallMaxWidth;
  final double tabletLargeMaxWidth;

  Breakpoints(
    this.smartphoneMaxWidth,
    this.tabletSmallMaxWidth,
    this.tabletLargeMaxWidth,
  );

  //! In case a platform has way more logical pixels than the other ones,
  //! add the chain of that platform to the [matchPhysicalPlatform] below.

  @override
  bool isSmartphone(double width) => lessThan(width, smartphoneMaxWidth);

  @override
  bool isTabletSmall(double width) => lessThan(width, tabletSmallMaxWidth);

  @override
  bool isTabletLarge(double width) => lessThan(width, tabletLargeMaxWidth);
}
