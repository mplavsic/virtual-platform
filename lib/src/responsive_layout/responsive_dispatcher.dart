import 'package:flutter/widgets.dart';

import '../abstract/utils/typedefs.dart';
import 'breakpoints.dart';
import '../abstract/utils/chain.dart';

class ResponsiveDispatcher extends StatelessWidget {
  final NullableBuilderOfWidget mobile;
  final NullableBuilderOfWidget smartphone;
  final NullableBuilderOfWidget tablet;
  final NullableBuilderOfWidget smallTablet;
  final NullableBuilderOfWidget largeTablet;
  final NullableBuilderOfWidget desktop;
  final BuilderOfWidget other;

  const ResponsiveDispatcher({
    super.key,
    required this.other,
    this.desktop,
    this.largeTablet,
    this.smallTablet,
    this.tablet,
    this.smartphone,
    this.mobile,
  });

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final NullableBuilderOfWidget caseChain;
    if (breakPoints.isSmartphone(width)) {
      caseChain = chain([smartphone, mobile]);
    } else if (breakPoints.isTabletSmall(width)) {
      caseChain = chain([smallTablet, tablet, mobile]);
    } else if (breakPoints.isTabletLarge(width)) {
      caseChain = chain([largeTablet, tablet, mobile]);
    } else {
      caseChain = chain([desktop]);
    }
    return chain([caseChain, other])!.call();
  }
}
