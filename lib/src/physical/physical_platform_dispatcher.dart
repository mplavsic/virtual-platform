import 'package:flutter/widgets.dart';

import 'match_physical_platform.dart';
import '../abstract/utils/typedefs.dart';

/// Builds a [Widget] based on the physical platform.
class PhysicalPlatformDispatcher extends StatelessWidget {
  final BuilderOfWidget other;
  final BuilderOfWidget? android;
  final BuilderOfWidget? fuchsia;
  final BuilderOfWidget? ios;
  final BuilderOfWidget? linux;
  final BuilderOfWidget? macos;
  final BuilderOfWidget? windows;
  final BuilderOfWidget? web;
  final BuilderOfWidget? appleSystems;
  final BuilderOfWidget? desktopSystems;
  final BuilderOfWidget? mobileSystems;

  /// Selects the widget builder to invoke that matches the physical platform.
  /// Use this builder for ***platform-specific Widgets***. Example: iOS devices
  /// might use the official webview (flutter_webview), while all other devices
  /// might need a different one (flutter_inappwebview).
  ///
  /// **[other] chain**
  ///
  /// [other] will be selected as default chain in case all
  /// other chains do not match.
  ///
  /// However, note that specifying [other] does not automatically bring safety,
  /// i.e.,
  /// a crash might occur even if the [other] chain is specified. For example,
  /// let us assume the virtual platform is [iosVirtualPlatform] and the widget
  /// tree consists of Cupertino widgets.
  /// If the matched chain is [other] and this chain builds a widget using the
  /// Material design, this widget should be wrapped in a [Material] before
  /// getting added to the "Cupertino" widget tree. If not, it might result
  /// in a crash.
  ///
  /// **Platform groups**
  ///
  /// [appleSystems], [desktopSystems] and [mobileSystems] are platform groups.
  ///
  /// - [mobileSystems] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktopSystems] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [appleSystems] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [appleSystems] has precedence
  /// over both [desktopSystems] and [mobileSystems].
  const PhysicalPlatformDispatcher({
    super.key,
    required this.other,
    this.android,
    this.fuchsia,
    this.ios,
    this.linux,
    this.macos,
    this.windows,
    this.web,
    this.appleSystems,
    this.desktopSystems,
    this.mobileSystems,
  });

  @override
  Widget build(BuildContext context) {
    return matchPhysicalPlatform(
      other: other,
      android: android,
      appleSystems: appleSystems,
      desktopSystems: desktopSystems,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      mobileSystems: mobileSystems,
      web: web,
      windows: windows,
    );
  }
}
