import 'dart:io';

import 'package:flutter/foundation.dart';

import '../abstract/match_abstract_platform.dart';
import '../abstract/utils/typedefs.dart';

/// Immediately invokes a function matching the ***physical*** platform.
/// Use this function for platform-specific ***business logic***, e.g.,
/// path to database.
///
/// **[other] chain**
///
/// [other] will be selected as default chain at runtime
/// in case the running platform (or a platform group containing it) is not
/// specified.
///
/// **Platform groups**
///
/// [appleSystems], [desktopSystems] and [mobileSystems] are platform groups.
///
/// - [mobileSystems] will be selected if, at runtime, the platform is one of:
///   - [android]
///   - [ios]
/// - [desktopSystems] will be selected if, at runtime, the platform is one of:
///   - [linux]
///   - [macos]
///   - [windows]
/// - [appleSystems] will be selected if, at runtime, the platform is one of:
///   - [ios]
///   - [macos]
///
/// Platforms have precedence over platform groups. [appleSystems] has precedence
/// over both [desktopSystems] and [mobileSystems].
T matchPhysicalPlatform<T>({
  BuilderOf<T>? android,
  BuilderOf<T>? fuchsia,
  BuilderOf<T>? ios,
  BuilderOf<T>? linux,
  BuilderOf<T>? macos,
  BuilderOf<T>? windows,
  BuilderOf<T>? web,
  BuilderOf<T>? appleSystems,
  BuilderOf<T>? desktopSystems,
  BuilderOf<T>? mobileSystems,
  required BuilderOf<T> other,
}) =>
    matchAbstractPlatform(
      isWeb: () => kIsWeb,
      isAndroid: () => Platform.isAndroid,
      isIos: () => Platform.isIOS,
      isFuchsia: () => Platform.isFuchsia,
      isLinux: () => Platform.isLinux,
      isMacos: () => Platform.isMacOS,
      isWindows: () => Platform.isWindows,
      android: android,
      appleSystems: appleSystems,
      desktopSystems: desktopSystems,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      mobileSystems: mobileSystems,
      other: other,
      web: web,
      windows: windows,
    );
