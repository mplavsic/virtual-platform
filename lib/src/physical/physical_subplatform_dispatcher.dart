import 'package:flutter/material.dart' show Material;
import 'package:flutter/widgets.dart';

import '../../physical_platform.dart';
import '../virtual/virtual_platform.dart';
import '../abstract/utils/typedefs.dart';

/// Builds a [Widget] based on the virtual platform and the logical pixels the
/// flutter app takes fetched by [MediaQuery].
class PhysicalSubplatformDispatcher extends StatelessWidget {
  /// If true, huge Android screens (e.g. external monitors) will be treated as tablets.
  final bool treatAndroidDesktopAsTabletLarge;

  /// If true, huge iOS screens (e.g. external monitors) will be treated as tablets.
  final bool treatIosDesktopAsTabletLarge;

  /// If false, the chains for mobile devices won't be consumed by this platform.
  final bool treatSmallLinuxAsMobile;

  /// If false, the chains for mobile devices won't be consumed by this platform.
  final bool treatSmallMacosAsMobile;

  /// If false, the chains for mobile devices won't be consumed by this platform.
  final bool treatSmallWindowsAsMobile;
  final BuilderOfWidget other;
  final BuilderOfWidget? android;
  final BuilderOfWidget? fuchsia;
  final BuilderOfWidget? ios;
  final BuilderOfWidget? linux;
  final BuilderOfWidget? linuxSmartphone;
  final BuilderOfWidget? linuxTablet;
  final BuilderOfWidget? linuxTabletSmall;
  final BuilderOfWidget? linuxTabletLarge;
  final BuilderOfWidget? linuxDesktop;
  final BuilderOfWidget? macos;
  final BuilderOfWidget? macosSmartphone;
  final BuilderOfWidget? macosTablet;
  final BuilderOfWidget? macosTabletSmall;
  final BuilderOfWidget? macosTabletLarge;
  final BuilderOfWidget? macosDesktop;
  final BuilderOfWidget? windows;
  final BuilderOfWidget? windowsSmartphone;
  final BuilderOfWidget? windowsTablet;
  final BuilderOfWidget? windowsTabletSmall;
  final BuilderOfWidget? windowsTabletLarge;
  final BuilderOfWidget? windowsDesktop;
  final BuilderOfWidget? web;
  final BuilderOfWidget? iphone;
  final BuilderOfWidget? ipad;
  final BuilderOfWidget? ipadSmall;
  final BuilderOfWidget? ipadLarge;
  final BuilderOfWidget? iosDesktop;
  final BuilderOfWidget? androidSmartphone;
  final BuilderOfWidget? androidTablet;
  final BuilderOfWidget? androidTabletSmall;
  final BuilderOfWidget? androidTabletLarge;
  final BuilderOfWidget? androidDesktop;
  final BuilderOfWidget? webSmartphone;
  final BuilderOfWidget? webTablet;
  final BuilderOfWidget? webTabletSmall;
  final BuilderOfWidget? webTabletLarge;
  final BuilderOfWidget? webDesktop;
  final BuilderOfWidget? appleSystems;
  final BuilderOfWidget? desktop;
  final BuilderOfWidget? mobile;
  final BuilderOfWidget? tablet;
  final BuilderOfWidget? tabletSmall;
  final BuilderOfWidget? tabletLarge;
  final BuilderOfWidget? otherSmartphone;
  final BuilderOfWidget? otherTablet;
  final BuilderOfWidget? otherTabletSmall;
  final BuilderOfWidget? otherTabletLarge;
  final BuilderOfWidget? otherDesktop;

  /// Builds a [Widget] based on the virtual platform and the logical pixels the
  /// flutter app takes fetched by [MediaQuery].
  ///
  /// The widget should only be used for platform-specific UI. Example: iPads
  /// might use the official webview (flutter_webview), while Android tablets
  /// might need a different one (flutter_inappwebview). iPhones and Android
  /// smartphone might display a [Text] widget stating that the screen is too
  /// small.
  ///
  /// Since there is no way to infer one of the above subplatforms
  /// other than relying on screen size dimension,
  /// you should ***NEVER*** rely on this widget for business logic differences
  /// (such as the path to the database)
  /// between different sizes of the same platform, e.g., [androidSmartphone]
  /// and [androidTablet].
  /// The screen dimension could change at runtime (through split view, or
  /// window resizing), resulting in a virtual platform switch, e.g.,
  /// from [androidSmartphone] to [androidTablet].
  ///
  /// **[other] chain**
  ///
  /// [other] will be selected as default chain in case all
  /// other chains do not match.
  ///
  /// However, note that specifying [other] does not automatically bring safety,
  /// i.e.,
  /// a crash might occur even if the [other] chain is specified. For example,
  /// let us assume the virtual platform is [iosVirtualPlatform] and the widget
  /// tree consists of Cupertino widgets.
  /// If the matched chain is [other] and this chain builds a widget using the
  /// Material design, this widget should be wrapped in a [Material] before
  /// getting added to the widget tree. If not, it might result in a crash.
  ///
  /// **Platform groups and subplatforms**
  ///
  /// To find out all possible combinations, the
  /// [documentation](https://gitlab.com/mplavsic/virtual-platform)
  /// should be consulted.
  const PhysicalSubplatformDispatcher({
    Key? key,
    this.treatAndroidDesktopAsTabletLarge = true,
    this.treatIosDesktopAsTabletLarge = true,
    this.treatSmallLinuxAsMobile = true,
    this.treatSmallMacosAsMobile = true,
    this.treatSmallWindowsAsMobile = true,
    required this.other,
    this.android,
    this.androidSmartphone,
    this.androidTablet,
    this.androidTabletSmall,
    this.androidTabletLarge,
    this.androidDesktop,
    this.fuchsia,
    this.ios,
    this.iphone,
    this.ipad,
    this.ipadSmall,
    this.ipadLarge,
    this.iosDesktop,
    this.linux,
    this.linuxSmartphone,
    this.linuxTablet,
    this.linuxTabletSmall,
    this.linuxTabletLarge,
    this.linuxDesktop,
    this.macos,
    this.macosSmartphone,
    this.macosTablet,
    this.macosTabletSmall,
    this.macosTabletLarge,
    this.macosDesktop,
    this.windows,
    this.windowsSmartphone,
    this.windowsTablet,
    this.windowsTabletSmall,
    this.windowsTabletLarge,
    this.windowsDesktop,
    this.web,
    this.webSmartphone,
    this.webTablet,
    this.webTabletSmall,
    this.webTabletLarge,
    this.webDesktop,
    this.appleSystems,
    this.desktop,
    this.mobile,
    this.tablet,
    this.tabletLarge,
    this.tabletSmall,
    this.otherSmartphone,
    this.otherTablet,
    this.otherTabletSmall,
    this.otherTabletLarge,
    this.otherDesktop,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => matchPhysicalSubplatform(
        context,
        treatAndroidDesktopAsTabletLarge: treatAndroidDesktopAsTabletLarge,
        treatIosDesktopAsTabletLarge: treatIosDesktopAsTabletLarge,
        treatSmallLinuxAsMobile: treatSmallLinuxAsMobile,
        treatSmallMacosAsMobile: treatSmallMacosAsMobile,
        treatSmallWindowsAsMobile: treatSmallWindowsAsMobile,
        other: other,
        android: android,
        androidSmartphone: androidSmartphone,
        androidTablet: androidTablet,
        androidTabletSmall: androidTabletSmall,
        androidTabletLarge: androidTabletLarge,
        androidDesktop: androidDesktop,
        appleSystems: appleSystems,
        desktop: desktop,
        fuchsia: fuchsia,
        ios: ios,
        ipad: ipad,
        ipadSmall: ipadSmall,
        ipadLarge: ipadLarge,
        iosDesktop: iosDesktop,
        iphone: iphone,
        linux: linux,
        linuxSmartphone: linuxSmartphone,
        linuxTablet: linuxTablet,
        linuxTabletSmall: linuxTabletSmall,
        linuxTabletLarge: linuxTabletLarge,
        linuxDesktop: linuxDesktop,
        macos: macos,
        macosSmartphone: macosSmartphone,
        macosTablet: macosTablet,
        macosTabletSmall: macosTabletSmall,
        macosTabletLarge: macosTabletLarge,
        macosDesktop: macosDesktop,
        mobile: mobile,
        tablet: tablet,
        tabletLarge: tabletLarge,
        tabletSmall: tabletSmall,
        web: web,
        webDesktop: webDesktop,
        webTablet: webTablet,
        webTabletSmall: webTabletSmall,
        webTabletLarge: webTabletLarge,
        webSmartphone: webSmartphone,
        windows: windows,
        windowsSmartphone: windowsSmartphone,
        windowsTablet: windowsTablet,
        windowsTabletSmall: windowsTabletSmall,
        windowsTabletLarge: windowsTabletLarge,
        windowsDesktop: windowsDesktop,
        otherSmartphone: otherSmartphone,
        otherTablet: otherTablet,
        otherTabletSmall: otherTabletSmall,
        otherTabletLarge: otherTabletLarge,
        otherDesktop: otherDesktop,
      );
}
