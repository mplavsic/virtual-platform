/// A library that simplifies the cross-platform development process by
/// providing declarative instruments that interact with the physical platform.
library physical_platform;

export 'src/physical/match_physical_platform.dart';
export 'src/physical/match_physical_subplatform.dart';
export 'src/physical/physical_platform_dispatcher.dart';
export 'src/physical/physical_subplatform_dispatcher.dart';
