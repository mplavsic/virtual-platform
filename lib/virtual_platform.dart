/// A package that simplifies the cross-platform development process by
/// providing a virtual platform than can be changed at runtime.
library virtual_platform;

export 'src/virtual/match_virtual_platform.dart';
export 'src/virtual/virtual_subplatform_dispatcher.dart';
export 'src/virtual/match_virtual_subplatform.dart';
export 'src/virtual/virtual_platform_dispatcher.dart';
export 'src/virtual/virtual_platform_notifier.dart';
export 'src/virtual/virtual_platform.dart';
